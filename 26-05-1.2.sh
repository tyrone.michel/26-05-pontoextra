#!/bin/bash

if [ -e $1 ]; then
	echo "OK"
else
	echo "Erro! Arquivo não encontrado"
	exit 1
fi

echo "1 para 10 primeiras linhas"
echo "2 para 10 últimas linhas"
echo "3 para Número de linhas"
echo "Faça usa escolha"

read escolha

if [ $escolha = "1" ]; then
echo "Você escolheu opção 1, para 10 primeiras linhas"
echo -e "Exibindo resultado abaixo\n"
	head -10 $1
elif [ $escolha = "2" ]; then
echo "Você escolheu opção 2, para 10 últimas linhas"
echo -e "Exibindo resultado abaixo\n"
	tail -10 $1
elif [ $escolha = "3" ]; then
echo "Você escolheu opção 3, para Número de linhas"
echo -e "Exibindo resultado abaixo\n"
	wc -l $1
else
	echo "Errou! Escolha entre 1, 2 ou 3"
fi
